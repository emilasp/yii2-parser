<?php

namespace emilasp\parser;

use emilasp\core\CoreModule;

/**
 * Class ParserModule
 * @package emilasp\parser
 */
class ParserModule extends CoreModule
{
    public $controllerNamespace = 'emilasp\parser\commands';
}
