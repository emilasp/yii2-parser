<?php

namespace emilasp\parser\commands;

use DateTime;
use emilasp\cms\common\models\Article;
use emilasp\core\commands\AbstractConsoleController;
use Yii;


/**
 *  Скрипт публикует статьи
 * Запуск по крону раз в час
 *
 * @package emilasp\parser\commands
 */
class ParseController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionIndex(): void
    {
        $this->display("-----Content publication-----", self::FONT_COLOR_YELLOW);

    }
}
