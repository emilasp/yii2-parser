<?php

namespace emilasp\parser\components;

use emilasp\taxonomy\models\Category;
use Yii;
use yii\base\Component;
use yii\helpers\Url;

/**
 * Компонент парсера
 *
 * Class ParserComponent
 * @package emilasp\parser\components
 */
class ParserComponent extends Component
{
    public const TYPE_URL_ITERATOR = 1;
    public const TYPE_URL_REGEXP = 2;
    public const TYPE_ITEM = 3;

    public $pagination = [];
    public $stages = [];

    public function run(): void
    {

    }
}
